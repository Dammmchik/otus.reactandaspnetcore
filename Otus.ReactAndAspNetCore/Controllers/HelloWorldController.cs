﻿using Microsoft.AspNetCore.Mvc;

namespace ReactAndAspNetCore.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HelloWorldController : ControllerBase
    {

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(new { Text = "Hello World!!!" });
        }
    }
}
